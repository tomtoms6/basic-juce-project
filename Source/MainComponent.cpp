/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    textButton1.setButtonText("Click Me");
    addAndMakeVisible(&textButton1);
    textButton1.addListener(this);
    
    textButton2.setButtonText("Click Me");
    addAndMakeVisible(&textButton2);
    textButton2.addListener(this);
    
    slider1.setSliderStyle(Slider::Rotary);
    addAndMakeVisible(slider1);
    slider1.addListener(this);
    
    combobox1.addItem("Item1", 1);
    combobox1.addItem("Item2", 2);
    combobox1.addItem("Item3", 3);
    addAndMakeVisible(combobox1);
    
    textEditor1.setText("Add some text here");
    addAndMakeVisible(textEditor1);

}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{

    DBG("MainComponent " << getHeight() << " " << getWidth());
    textButton1.setBounds(10,10, getWidth() - 20, 40);
    textButton2.setBounds(10,170, getWidth() - 20, 40);
    slider1.setBounds(10, 50, getWidth() - 20, 40);
    combobox1.setBounds(10, 90, getWidth() - 20, 40);
    textEditor1.setBounds(10, 130, getWidth() - 20, 40);
}

void MainComponent::buttonClicked(Button* button)
{
    if (button == &textButton1)
    DBG("Stop Clicking me");
    else if (button == &textButton2)
    DBG("Ohh yeah thats nice!");
    
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    DBG("sVal");
}

